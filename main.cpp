#include <iostream>
#include "pessoa.hpp"

using namespace std;

int main(int argc, char ** argv){
 
  Pessoa pessoa1;
  Pessoa * pessoa2 = new Pessoa();

  pessoa1.setNome("João");
  pessoa1.setMatricula(4588);

  pessoa2->setNome("Maria");
  pessoa2->setMatricula(180006802);

  cout << "Pessoa 1 " << endl;
  cout << "Nome: " << pessoa1.getNome() << endl;
  cout << "Matricula: " << pessoa1.getMatricula() << endl;

  cout << "Pessoa 2 " << endl;
  cout << "Nome: " << pessoa2->getNome() << endl;
  cout << "Matricula: " << pessoa2->getMatricula() << endl;

  delete pessoa2;

}

#ifndef ALUNO_HPP
#define ALUNO_HPP

#include <string>

using namespace std;

class Aluno : public Pessoa(){

  //Atributos
private:
  int creditos;
  float ira;
  int semestre;
  string curso;

  //Métodos
public:
  Aluno();
  ~Aluno();
  void setCreditos(int creditos);
  int getCreditos();
  void setIRA(float ira);
  float getIRA();
  void setSemestre(int semestre);
  int getSemestre();
  void setCurso(string curso);
  string getCurso();

  //Outros
  void imprimeDados();
};

#endif

#include <iostream>
#include "aluno.hpp"

Aluno::Aluno(){
  cout << "Construtor da classe aluno" << endl;

  nome="";
  sexo="";
  RG=0;
  CPF=0;
  email="";
  matricula=0;
  creditos=0;
  ira=5.0;
  semestre=1;
  curso="";
}

Aluno::~Aluno(){
  cout << "Destrutor da classe Aluno" << endl;
}

void Aluno::setCreditos(int creditos){
  this->creditos=creditos;
}

int Aluno::getCreditos(){
  return creditos;
}

void Aluno::setCreditos(int creditos){
  this->creditos=creditos;
}

int Aluno::getCreditos(){
  return creditos;
}

void Aluno::setIRA(float ira){
  this->ira=ira;
}

int Aluno::getIRA(){
  return ira;
}

void Aluno::setSemestre(int semestre){
  this->semestre=semestre;
}

int Aluno::getSemestre(){
  return semestre;
}

void Aluno::setCurso(string curso){
  this->curso=curso;
}

string Aluno::getCurso(){
  return curso;
}

void Aluno:: imprimeDados(){
  cout << "Nome: " << nome << endl;
  cout << "Sexo: " << sexo << endl;
  cout << "RG: " << RG << endl;
  cout << "CPF: " << CPF << endl;
  cout << "Email: " << email << endl;
  cout << "Matrícula: " << matricula << endl;
  cout << "Creditos: " << creditos << endl;
  cout << "IRA: " << ira << endl;
  cout << "Semestre: " << semestre << endl;
  cout << "Curso: " << curso << endl;
}

